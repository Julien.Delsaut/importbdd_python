import csv
import os
import sqlite3
import logging

def connexion_bdd(csv_para, bdd_para):
    connection = sqlite3.connect(bdd_para, timeout = 10)
    #Initialisation de la BDD et connexion
    cursor = connection.cursor()
    #Initialisation des fichiers Logging
    logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s')

    #Ouverture pour commencer la lecture du csv
    with open(csv_para) as fichiercsv:
        #On commence la lecture à la Ligne 1
        for liste_de_base in list(csv.reader(fichiercsv, delimiter = ';'))[1:]:
                #Initialisation de l'index pour l'immatriculation
                immat = liste_de_base[3]
                #Instruction SQL vérifiant si l'immat est déjà présente
                cursor.execute('SELECT immatriculation FROM auto WHERE immatriculation = ?', (immat,))
#               #Si l'immat est nouvelle alors il insert la donnée, sinon il met à jour sa nouvelle valeur
                try:
                        if(cursor.fetchone() is None):
                                cursor.execute('INSERT INTO auto (adresse_titulaire, nom, prenom, immatriculation, date_immatriculation, marque, denomination_commerciale, couleur, carrosserie, categorie, cylindree, energie, places, poids, puissance, type, variante, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', liste_de_base)
                        else: 
                                liste = liste_de_base
                                liste.append(immat)
                                cursor.execute('UPDATE auto SET adresse_titulaire = ?, nom = ?, prenom = ?, immatriculation = ?, date_immatriculation = ?, marque = ?, denomination_commerciale = ?, couleur = ?, carrosserie = ?, categorie = ?, cylindree = ?, energie = ?, places = ?, poids = ?, puissance = ?, type = ?, variante = ?, version = ? WHERE immatriculation = ?;', liste)   
                except sqlite3.ProgrammingError as e:
                        logging.error("Nombre de données dans le csv est incorrect")
                        logging.info("Le programme ne s'est pas execute correctement")
                        connection.commit()
                        connection.close()
                        logging.info("Fermeture de la BDD...")
                        exit(3)
#                                                                                                                   #
        logging.info("les donnees ont ete ajoutees a la BBD")
         #Logging des changements
        connection.commit()
    #Clos la connexion envers la BDD
    connection.close()
    logging.info("Fermeture de la BDD...")