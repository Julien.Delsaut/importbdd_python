import sqlite3
import logging 

#Fonction de vérification de la BDD passé en paramètre.
def verification_BDD(bdd_para):
    #Identification du logging de connexion à la BDD
    logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s')
    
    #Création de la base de donnée
    connection = sqlite3.connect(bdd_para)
    logging.info("La connexion à la base de donnée est entrain de s'effectuer....")
    # création du cursor de la BDD
    cursor = connection.cursor()

    #Création de la table de la base de donnée avec la totalité des colonnes:
    try:
        cursor.execute('''CREATE TABLE auto(
        adresse_titulaire text,
        nom text,
        prenom text,
        immatriculation text,
        date_immatriculation text,
        marque text,
        denomination_commerciale text,
        couleur text,
        carrosserie text,
        categorie text,
        cylindree text,
        energie integer,
        places integer,
        poids integer,
        puissance integer,
        type integer,
        variante text,
        version text)
    ''')
        logging.info("Les tables sont créées !")
    except sqlite3.OperationalError as e:
        logging.warning(e)
    #Mise à jour des changements
    connection.commit()
    #Déconnexion de la base de donnée
    connection.close()
    return bdd_para