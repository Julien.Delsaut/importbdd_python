import unittest
from Functions.verification_bdd import verification_BDD
from Functions.connexion_bdd import connexion_bdd


class testAuto(unittest.TestCase):
    # test du découpage
    def test_existence_bdd(self):
        test_bdd = "bdd/testbdd"
        self.assertEqual(verification_BDD(test_bdd), "bdd/testbdd")

