import sqlite3
import logging
import os
import csv
import sys
import atexit
import argparse
from Functions.verification_bdd import verification_BDD
from Functions.connexion_bdd import connexion_bdd

logging.basicConfig(filename='logging.log', level = logging.DEBUG, format ='%(asctime)s %(message)s')
logging.info("Le programme vient de se lancer")

#Test du nombre d'argument rentré
if(len(sys.argv) != 3): 
    logging.warning("Le nombre d'arguments est incorrect")

 #Stock les arguments passer en paramètre dans les variables
try: 
    csv_para = sys.argv[1]
    bdd_para = sys.argv[2]
except IndexError as e:
    logging.error(e)
    logging.info("Le programme a rencontré une erreur lors de son éxécution")
    exit(1)

# vérifie la présence du fichier csv passé en paramètre
try:
    with open(csv_para, 'r+') as fichiercsv:
        logging.info("Connexion au csv...")
except FileNotFoundError as e:
    logging.error(e)
    logging.info("Le programme a rencontré une erreur lors de son éxécution")
    exit(2)

#Appel de la fonction connexion_Bdd
connexion_bdd(csv_para,bdd_para)
#Appel de la fonction stock_bdd
verification_BDD(bdd_para)